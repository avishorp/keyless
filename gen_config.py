#!/usr/bin/env python3

import sys
import json
import random
import base64
import re
import os.path

import yaml

USAGE = """
Generates ESPHome configuration files

gen_config.py [Config filename]
"""

DEFAULT_CONFIG_FILENAME = "keyless-config.yaml"
GEN_FILENAME = ".generated"
BUTTON_FILENAME = "keyless-button.yaml"
RINGER_FILENAME = "keyless-ringer.yaml"


def use_or_generate(source_dict, key, generator):
    val = source_dict[key]
    if val.strip() == "!generate":
        try:
            generated = json.load(open(GEN_FILENAME, "r"))
            return generated[key]
        except Exception:
            return generator()
    else:
        return val

def api_key_generator():
    key = random.randbytes(32)
    return base64.b64encode(key).decode()

def ota_key_generator():
    key = random.randbytes(16)
    return "".join([f"{b:02x}" for b in key])

def replace_variables(string, dictionary):
    # Regular expression pattern to match '${varname}'
    pattern = r'\${([^}]*)}'

    # Define a function to be used as a replacement callback
    def replace(match):
        varname = match.group(1)  # Extract varname from the match
        return dictionary.get(varname, match.group(0))  # Return value from dictionary, or original match if not found

    # Use re.sub with the defined function as the replacement
    return re.sub(pattern, replace, string)

def main():
    # Read and parse the configuration file
    if len(sys.argv) == 1:
        config_filename = DEFAULT_CONFIG_FILENAME
    elif len(sys.argv) == 2:
        config_filename = sys.argv[1]
    else:
        print(USAGE)
        sys.exit(1)

    config_data = yaml.load(open(config_filename, "r"), Loader=yaml.BaseLoader)
    settings = config_data["settings"]
    api_key_button = use_or_generate(settings, "api_key_button", api_key_generator)
    ota_key_button = use_or_generate(settings, "ota_key_button", ota_key_generator)
    api_key_ringer = use_or_generate(settings, "api_key_ringer", api_key_generator)
    ota_key_ringer = use_or_generate(settings, "ota_key_ringer", ota_key_generator)
    min_rssi = settings["min_rssi"]
    uuids = config_data["uuids"]

    ble_lines = []
    for uuid_index, uuid in enumerate(uuids):
        ble_lines.append(f"  - platform: ble_presence")
        ble_lines.append(f"    id: key{uuid_index}")
        ble_lines.append(f"    ibeacon_uuid: \"{uuid}\"")
        ble_lines.append(f"    min_rssi: {min_rssi}")
        ble_lines.append(f"    timeout: 5s")
    ble_presence_instances = "\n".join(ble_lines)

    authorized_lambda = "||".join([f"id(key{n}).state" for n in range(len(uuids))])
    
    all_conf = {**settings,
        "ble_presence_instances": ble_presence_instances,
        "authorized_lambda": authorized_lambda,
        "api_key_button": api_key_button,
        "ota_key_button": ota_key_button,
        "api_key_ringer": api_key_ringer,
        "ota_key_ringer": ota_key_ringer
        }

    button_template_filename = os.path.join("esphome", BUTTON_FILENAME + ".in")
    button_template = open(button_template_filename, "r").read()
    button_config = replace_variables(button_template, all_conf)
    open(BUTTON_FILENAME, "w").write(button_config)
       
    ringer_template_filename = os.path.join("esphome", RINGER_FILENAME + ".in")
    ringer_template = open(ringer_template_filename, "r").read()
    ringer_config = replace_variables(ringer_template, all_conf)
    open(RINGER_FILENAME, "w").write(ringer_config)

if __name__ == "__main__":
    main()

