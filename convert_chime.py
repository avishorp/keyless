#!/usr/bin/env python3

import sys
import subprocess
import tempfile

def main():
    source = sys.argv[1]
      
    with tempfile.NamedTemporaryFile("rb") as pcm_file:
        ffmpeg_args = [
            "ffmpeg",
            "-i",
            source,
            "-ac",
            "1",
            "-ar",
            "8000",
            "-acodec",
            "pcm_s8",
            "-f",
            "s8",
            "-y",
            pcm_file.name
        ]

        r = subprocess.run(ffmpeg_args)
        if r.returncode != 0:
            print("Failed converting the input file")
            return 1
        
        pcm_data = pcm_file.read()
        pcm_data_text = "[" + ",".join([str(b) for b in pcm_data]) + "]"
        
        open("chime.yaml", "w").write(pcm_data_text)
        print("File successfully converted")
    
    return 0

if __name__ == "__main__":
    sys.exit(main())

