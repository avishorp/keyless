# Keyless Home Entry System

## Description

The keyless home entry system enables access to one's home without the 
need for a traditional key. It operates through a cellular phone, 
identifying individuals authorized to enter the secured premises. Upon 
detecting such a phone, the doorbell button undergoes a functional 
shift. Rather than signaling with a traditional ring, it communicates 
(via Home Assistant) with an electronic lock, either unlocking or 
locking the door as required. In the absence of an authorized phone, the
 doorbell button maintains its original purpose, signaling with a 
conventional ring. This setup achieves two primary objectives: firstly, 
the door can be effortlessly unlocked with a simple button press, 
eliminating the necessity for keys, PIN codes, or fingerprints. 
Secondly, the entire system remains concealed; potential attackers are 
unaware of its existence or the presence of an electronic lock on the 
door.

## How it works

<img src="doc/concept.svg" title="" alt="" data-align="center">

An electonic unit, mounted inside the doorbell button senses a nearby authorized phone. It does it by listening to a iBeacon that the phone emits. An application should run on an authorized phone that causes it to constantly emit an iBeacon. Such application can be, for example, the Home Assistant Companion App. Each beacon has a  unique ID, which is listed in the button's unit firmware. When an appropriate beacon is received and decoded, an LED changes its color. Now, when the user short presses the button, it sends an unlock command to a Home Assistant instance which causes the door to unlock. When the user presse the button for more than two seconds, a lock command is sent.

When no authorized phone is nearby, the button unit publishes an MQTT message. This message is received by the indoor unit which plays a ringing sound. The MQTT message can be listened by multiple indoor units or by any party who is willing to react to such event (send a phone notification, for example).

## Hardware

Usually, a doorbell system comprises of an indoor unit to which an outdoor button is connected by two wires. In order to retain this setup and avoid having to install additional wires, the keyless entry system is made of two units. The first is installed inside the button box. It is built around an ESP32-S3 dev board, to which the button is connected, along with an RGB LED. The ESP32-S3 has a Bluetooth radio, through which it listens and detects a nearby phone. The LED changes its color when an authorized phone is detected, signalling to the user that the door can bu locked or unlocked.



<img src="doc/keyless-button.svg" title="" alt="" data-align="center">



The second unit replaces the doorbell unit. It is built around an ESP32 dev board (original ESP32 has much more RAM than the newer versions, which is essential here), an I2S DAC board with an integral amplifier and a small speaker. Additionally, the indoor unit has a small power supply unit that supplies 5V for both units, via the wire pair connecting them. Here, a HI-LINK module is used, but essentially any power supply unit that can source ~600mA of 5V can be used.

<img src="doc/keyless-ringer.png" title="" alt="" data-align="center">

## Software

The software driving both the button unit and the indoor unit is ESPHome. A recent (2024.4 or newer) version is **required**. 

The software is prepared and installed on the target in two steps. First, the YAML files descibing the configuration should be modified to adapt to some specific site-specific settings. A special script is provided in order to help with this task. Then, the initial flashing is performed using ESPHome tools. The most convenient way to do that is to use the ESPHome Home Assistant add-on. Provided that the WiFi network settings are correct, the units can be updated over-the-air from that point on.

### Installation Instructions

1. **Prerequisites:** A working Python installation (version 3.10 and above)

2. Clone or download the project's repository.

3. Create a virtual Python environment (recommended) and install `esphome`.

4. Review the `keyless-config.yaml` file and change the settings as required. The file contains inline comments describing each option.

5. Run `python gen_config.py`. This will generate two YAML files: `keyless-button.yaml` and `keyless-ringer.yaml`.

6. Compile the button firmware: `esphome compile keyless-button.yaml`

7. Connect the hardware to the serial port and upload it: `esphome upload keyless-button.yaml`

8. Repeat steps 6 and 7 for the ringer.

## Customizations

### Changing GPIOs

The ESPHome YAML files are designed such that the GPIOs are connected as described in the sketches above. If different connections are desired, the YAML files should be modified to reflect it. The I2S bus on the indoor unit can also be mapped to any GPIO pins as desired.

### Changing the doorbell chime

The project is shipped with a "ding-dong" doorbell sound ([source]([DoorBell Shortened | Royalty-free Music - Pixabay](https://pixabay.com/sound-effects/doorbell-shortened-100308/))). This sound can be changed in two ways.

The first one is to use the `convert_chime.py` script. Pass any short sound file as the first argument, and the script will convert it into a data file named `chime.yaml`, which is used in the build process. The script uses `ffmpeg` which should be available for running.

The second method is to use the [rtttl buzzer]([Rtttl Buzzer &#8212; ESPHome](https://esphome.io/components/rtttl.html)) component of ESPHome. This component can be used to create short tunes described by text strings. Various examples can be found across the net. To user rtttl, add the following lines to `keyless-ringer.yaml.in`:

```yaml
rtttl:
  speaker: main_speaker
  id: my_rtttl
  gain: 0.8
```

Then change then, remove the line that start with `speaker.play` and replace it with `rtttl.play: 'siren:d=8,o=5,b=100:d,e,d,e,d,e,d,e'` (replacing the text with the one that gives the desired tune).




